<?php

return [
    'singular' => 'Package Directory',
    'plural' => 'Package Directory',
    'submit' => 'Submit Package',
    'item' => 'Package',
    'downloads' => 'Downloads',
    'latest' => 'Latest Version',
    'add_first' => 'Add your first package.',
];
