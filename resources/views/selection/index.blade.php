<x-dashboard title="Browse Repos">
    <x-heading>
        <div class="flex justify-between items-center">
            <h2 class="text-2xl font-medium leading-7 sm:text-3xl sm:leading-9 sm:truncate">
                Browse Repos
            </h2>
        </div>
    </x-heading>

    <livewire:repo-selector />
</x-dashboard>
