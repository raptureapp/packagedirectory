<x-dashboard :title="__('directory::package.plural')">
    <x-heading class="flex justify-between items-center">
        <div>
            <x-h1>@lang('directory::package.plural')</x-h1>

            @if ($auth)
                @if ($auth->isExpired())
                    <small style="display: block; font-size: 14px; line-height: 1.4em; margin-top: 0.25rem">Access Expired</small>
                @else
                    <small style="display: block; font-size: 14px; line-height: 1.4em; margin-top: 0.25rem">Connected: <strong>{{ $auth->username }}</strong></small>
                @endif
            @endif
        </div>

        @if ($auth)
            @can('directory.create', 'directory')
                @if ($auth->isExpired())
                    <x-button href="{{ route('dashboard.directory.reconnect') }}" color="primary">Reconnect</x-button>
                @else
                    <div class="flex items-center justify-end space-x-3">
                        <x-quick-create :action="route('dashboard.directory.store')" placeholder="https://bitbucket.org/raptureapp/core" name="url" icon="plus">
                            <span>@lang('directory::package.submit')</span>
                        </x-quick-create>

                        <x-button href="{{ route('dashboard.directory.selection') }}" icon="folders" color="primary">
                            <span>Browse</span>
                        </x-button>
                    </div>
                @endif
            @endcan
        @endif
    </x-heading>

    @if ($auth)
        <livewire:package-table />
    @else
        <x-container>
            <div class="shadow rounded bg-white p-4 text-center space-y-4">
                <x-button href="{{ route('dashboard.directory.login') }}" class="inline-flex items-center space-x-2" color="primary">
                    <em class="fab fa-bitbucket"></em>
                    <span>Connect Bitbucket</span>
                </x-button>
            </div>
        </x-container>
    @endif
</x-dashboard>
