<div>
    <x-statuses />

    <x-container>
        <div class="space-y-2">
            @if (!empty($viewing))
                <div class="rounded shadow bg-white text-gray-700 cursor-pointer" wire:click="returnToWorkspaces">
                    <div class="px-6 py-4 font-medium flex items-center space-x-4">
                        <em class="far fa-level-up"></em>
                        <span>Return to Workspaces</span>
                    </div>
                </div>
                @foreach ($repos as $repo)
                    <livewire:repo-toggle :exists="in_array($repo['full_name'], $existingRepos)" :repo="$repo" key="repo-{{ $repo['slug'] }}" />
                @endforeach
            @else
                @foreach ($workspaces as $workspace)
                    <div class="rounded shadow bg-white text-gray-700 cursor-pointer px-6 py-4 font-medium uppercase" wire:click="selectWorkspace('{{ $workspace['slug'] }}')" key="workspace-{{ $workspace['slug'] }}">
                        {{ $workspace['name'] }}
                    </div>
                @endforeach
            @endif
        </div>
    </x-container>
</div>
