<div class="rounded shadow text-gray-700 cursor-pointer px-6 py-4 leading-snug border {{ $errors->has($repo['full_name']) ? 'border-red-400 bg-red-100' : 'border-transparent bg-white' }}">
    <div class="flex items-center justify-between">
        <div>
            <strong>{{ $repo['name'] }}</strong><br>
            <span class="text-gray-500 text-sm">{{ $repo['full_name'] }}</span>
        </div>
        <div class="flex justify-end space-x-4">
            @error($repo['full_name'])
                <div>{{ $message }}</div>
            @enderror

            <div class="w-12 text-center text-lg" wire:loading>
                <em class="far fa-spinner fa-spin"></em>
            </div>
            <span role="checkbox" tabindex="0" class="relative inline-flex flex-shrink-0 h-6 w-12 py-1 px-1 rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring {{ $exists ? 'bg-primary-600' : 'bg-gray-400' }}" wire:click="syncRepo" wire:loading.remove>
                <span aria-hidden="true" class="inline-block h-full w-4 rounded-full bg-white shadow transform transition ease-in-out duration-200 {{ $exists ? 'translate-x-6' : 'translate-x-0' }}"></span>
            </span>
        </div>
    </div>
</div>
