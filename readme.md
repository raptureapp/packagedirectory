# Package Directory

Provide access to private repos within Bitbucket. Releases are automatically fetched upon push and hidden from the general public.

## Installation

### Step 1

Install the composer package:

`composer require rapture/package-directory`

After that has finished install the package into rapture:

`php artisan package:install rapture/package-directory`

### Step 2

Place the following snippet in `config/services.php`

```
'bitbucket' => [
    'client_id' => env('BITBUCKET_KEY'),
    'client_secret' => env('BITBUCKET_SECRET'),
    'redirect' => env('BITBUCKET_REDIRECT_URI')
],
```

### Step 3

1. Go to Bitbucket and click on your avatar in the bottom left hand corner of the screen.
2. Click on your workspace
3. Go to **Settings**
3. Under **Apps and Features** click on **OAuth consumers**
4. Click on **Add consumer**
5. Provide a name and add the callback URL `{yourDomain}/dashboard/login/bitbucket/callback`
6. You will need the following permissions:
    * account - email
    * account - read 
    * workspace membership - read
    * repositories - read
    * repositories - write
    * repositories - admin
    * webhooks - read and write
7. Save
8. Expand the newly created consumer
9. Copy both the key and secret

### Step 4

Using the values copied in Step 3. Add the following to the bottom of your `.env`

```
BITBUCKET_KEY={yourKey}
BITBUCKET_SECRET={yourSecret}
BITBUCKET_REDIRECT_URI={yourDomain}/dashboard/login/bitbucket/callback
```

### Step 5

Add `/usage` and `/repo/pushed` to the except variable in `app/Http/Middleware/VerifyCsrfToken.php`

## Usage

Add the following snippet to your composer.json replacing the URL with a link to the root of your project.

```
"repositories": [
    {
        "type": "composer",
        "url": "http://packages.example.org/"
    }
],
```
