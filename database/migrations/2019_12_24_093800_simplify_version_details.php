<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SimplifyVersionDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('package_versions', function (Blueprint $table) {
            $table->dropColumn('autoload');
            $table->dropColumn('require');
            $table->dropColumn('requiredev');
            $table->longtext('composer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('package_versions', function (Blueprint $table) {
            $table->dropColumn('composer');
            $table->text('autoload')->nullable();
            $table->text('require')->nullable();
            $table->text('requiredev')->nullable();
        });
    }
}
