<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['web', 'auth'])
    ->namespace('Rapture\Directory\Controllers')
    ->prefix('dashboard')
    ->name('dashboard.')
    ->group(function () {
        Route::resource('directory', 'DirectoryController')->only(['index', 'store', 'destroy']);
        Route::get('directory/reconnect', 'TokenController@store')->name('directory.reconnect');
        Route::get('directory/selection', 'SelectionController@index')->name('directory.selection');
        Route::get('login/bitbucket', 'LoginController@redirectToProvider')->name('directory.login');
        Route::get('login/bitbucket/callback', 'LoginController@handleProviderCallback')->name('directory.callback');
    });

Route::middleware(['web'])
    ->namespace('Rapture\Directory\Controllers')
    ->group(function () {
        Route::get('packages.json', 'PackageFeedController@index');
        Route::get('package/{namespace}/{package}/{version?}', 'DownloadPackageController@verify');
    });

Route::middleware(['api'])
    ->namespace('Rapture\Directory\Controllers')
    ->group(function () {
        Route::post('usage', 'RecordUsageController@store');
        Route::post('repo/pushed', 'WebhookController@store');
        Route::get('/p2/{workspace}/{package}.json', 'PackageFeedController@show');
    });
