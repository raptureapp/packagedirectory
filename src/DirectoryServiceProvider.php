<?php

namespace Rapture\Directory;

use Illuminate\Console\Scheduling\Schedule;
use Livewire\Livewire;
use Rapture\Directory\Jobs\RefreshToken;
use Rapture\Directory\Livewire\PackageTable;
use Rapture\Directory\Livewire\RepoSelector;
use Rapture\Directory\Livewire\RepoToggle;
use Rapture\Directory\Widgets\DownloadsOverTime;
use Rapture\Directory\Widgets\TotalDownloads;
use Rapture\Directory\Widgets\TotalPackages;
use Rapture\Directory\Widgets\TotalVersions;
use Rapture\Packages\Package;
use Rapture\Packages\Providers\PackageProvider;

class DirectoryServiceProvider extends PackageProvider
{
    public function configure(Package $package)
    {
        $package->name('directory', 'Package Hosting')
            ->description('Private composer packages')
            ->translations()
            ->migrations()
            ->routes('web')
            ->views()
            ->widget(TotalPackages::class)
            ->widget(TotalDownloads::class)
            ->widget(TotalVersions::class)
            ->widget(DownloadsOverTime::class)
            ->installer();
    }

    public function installed()
    {
        Livewire::component('package-table', PackageTable::class);
        Livewire::component('repo-selector', RepoSelector::class);
        Livewire::component('repo-toggle', RepoToggle::class);

        $this->callAfterResolving(Schedule::class, function (Schedule $schedule) {
            $schedule->job(new RefreshToken())->everyTwoHours();
        });
    }
}
