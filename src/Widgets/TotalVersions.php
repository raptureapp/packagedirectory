<?php

namespace Rapture\Directory\Widgets;

use Carbon\Carbon;
use Rapture\Core\Widgets\SingleStat;
use Rapture\Directory\Models\PackageVersion;

class TotalVersions extends SingleStat
{
    public $label = 'Total Versions';

    public function data()
    {
        return PackageVersion::count();
    }

    public function trend($data)
    {
        $thirtyDaysAgo = Carbon::now()->subDays(30);
        $recentDownloads = PackageVersion::where('created_at', '>=', $thirtyDaysAgo)->count();

        return $recentDownloads;
    }
}
