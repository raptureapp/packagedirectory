<?php

namespace Rapture\Directory\Widgets;

use Rapture\Core\Widgets\BarChart;
use Rapture\Directory\Models\PackageUse;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DownloadsOverTime extends BarChart
{
    public $label = 'Package Usage';

    public function data()
    {
        $endDate = Carbon::now()->endOfMonth();
        $startDate = $endDate->copy()->subYear();

        $priorDownloads = PackageUse::where('created_at', '<', $startDate)->count();

        $downloads = $this->getDownloadsData($startDate, $endDate);
        $cumulative = $priorDownloads;

        $cumulativeData = collect($this->getLastTwelveMonths())
            ->map(function ($month) use ($downloads, &$cumulative) {
                $monthKey = Carbon::parse($month)->format('Y-m');
                $monthDownloads = $downloads[$monthKey] ?? 0;
                $cumulative += $monthDownloads;
                return $cumulative;
            });

        return [[
            'name' => 'Downloads',
            'data' => $cumulativeData->values()->toArray()
        ]];
    }

    public function xAxis()
    {
        return $this->getLastTwelveMonths();
    }

    private function getDownloadsData($startDate, $endDate)
    {
        return PackageUse::select(DB::raw('DATE_FORMAT(created_at, "%Y-%m") as month, COUNT(*) as total'))
            ->where('created_at', '>=', $startDate)
            ->where('created_at', '<', $endDate)
            ->groupBy('month')
            ->orderBy('month')
            ->pluck('total', 'month')
            ->toArray();
    }

    private function getLastTwelveMonths()
    {
        return collect(range(11, 0))
            ->map(function ($months) {
                return Carbon::now()->startOfMonth()->subMonths($months)->format('M Y');
            })
            ->toArray();
    }
}
