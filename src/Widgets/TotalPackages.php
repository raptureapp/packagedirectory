<?php

namespace Rapture\Directory\Widgets;

use Rapture\Core\Widgets\SingleStat;
use Rapture\Directory\Models\Package;
use Carbon\Carbon;

class TotalPackages extends SingleStat
{
    public $label = 'Total Packages';

    public function data()
    {
        return Package::count();
    }

    public function trend($data)
    {
        $thirtyDaysAgo = Carbon::now()->subDays(30);
        $newPackages = Package::where('created_at', '>=', $thirtyDaysAgo)->count();

        return $newPackages;
    }
}
