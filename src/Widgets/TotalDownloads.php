<?php

namespace Rapture\Directory\Widgets;

use Rapture\Core\Widgets\SingleStat;
use Rapture\Directory\Models\PackageUse;
use Carbon\Carbon;

class TotalDownloads extends SingleStat
{
    public $label = 'Total Downloads';

    public function data()
    {
        return PackageUse::count();
    }

    public function trend($data)
    {
        $thirtyDaysAgo = Carbon::now()->subDays(30);
        $recentDownloads = PackageUse::where('created_at', '>=', $thirtyDaysAgo)->count();

        return $recentDownloads;
    }
}
