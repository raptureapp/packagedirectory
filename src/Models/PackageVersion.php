<?php

namespace Rapture\Directory\Models;

use Illuminate\Database\Eloquent\Model;

class PackageVersion extends Model
{
    protected $guarded = [];

    public function uses()
    {
        return $this->hasMany(PackageUse::class)->orderBy('created_at', 'desc');
    }
}
