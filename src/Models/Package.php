<?php

namespace Rapture\Directory\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Package extends Model
{
    use Searchable;

    protected $guarded = [];

    public function versions()
    {
        return $this->hasMany(PackageVersion::class)->orderBy('created_at', 'desc');
    }

    public function uses()
    {
        return $this->hasMany(PackageUse::class)->orderBy('created_at', 'desc');
    }

    public function scopeWithLatestVersion($query)
    {
        $query->addSelect(['latest_version' => PackageVersion::select('version')
            ->whereColumn('package_id', 'packages.id')
            ->where('version', '!=', 'dev-master')
            ->latest()
            ->take(1)
        ]);
    }

    public function repo()
    {
        return 'https://bitbucket.org/' . $this->source;
    }

    public function purge()
    {
        $this->versions()->delete();
        $this->uses()->delete();
        $this->delete();
    }

    public function toSearchableArray()
    {
        return [
            'name' => $this->name,
            'description' => $this->description,
            'source' => $this->source,
        ];
    }
}
