<?php

namespace Rapture\Directory\Models;

use Illuminate\Database\Eloquent\Model;

class Auth extends Model
{
    protected $guarded = [];

    protected $casts = [
        'expiration' => 'datetime',
    ];

    public function isExpired()
    {
        return $this->expiration < now();
    }
}
