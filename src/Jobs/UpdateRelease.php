<?php

namespace Rapture\Directory\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Rapture\Directory\Models\Auth;
use Rapture\Directory\Models\PackageVersion;

class UpdateRelease implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $package;
    protected $release;
    protected $target;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($package, $target, $release)
    {
        $this->package = $package;
        $this->target = $target;
        $this->release = $release;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $name = Str::uuid();
        $auth = Auth::first();
        $version = PackageVersion::where([
            'package_id' => $this->package->id,
            'version' => $this->release['version'],
        ])->first();

        $client = new Client([
            'base_uri' => 'https://api.bitbucket.org/2.0/repositories/',
            'headers' => [
                'Authorization' => 'Bearer ' . $auth->token,
                'Accept' => 'application/json',
            ],
        ]);

        $client->request('GET', $this->release['path'], [
            'sink' => storage_path('app/packages/' . $this->package->name . '/' . $name . '.zip'),
        ]);

        try {
            $response = $client->get($this->package->source . '/src/' . $this->target . '/composer.json');
        } catch (ClientException $e) {
            $response = null;
        }

        if (!is_null($response)) {
            $composer = json_decode($response->getBody()->getContents());
            $version->composer = json_encode($composer);
        }

        @unlink(storage_path($version->path));

        $version->path = 'app/packages/' . $this->package->name . '/' . $name . '.zip';
        $version->save();
    }
}
