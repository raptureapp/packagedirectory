<?php

namespace Rapture\Directory\Jobs;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Rapture\Directory\Models\Auth;

class PackageDelete implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable;

    protected $source;
    protected $hook;

    public function __construct($source, $hook)
    {
        $this->source = $source;
        $this->hook = $hook;
    }

    public function handle()
    {
        $auth = Auth::first();

        $client = new Client([
            'base_uri' => 'https://api.bitbucket.org/2.0/repositories/',
            'headers' => [
                'Authorization' => 'Bearer ' . $auth->token,
                'Accept' => 'application/json',
            ],
        ]);

        try {
            $hook = $client->delete($this->source . '/hooks/' . $this->hook_id);
        } catch (ClientException $e) {
            //
        }
    }
}
