<?php

namespace Rapture\Directory\Jobs;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Rapture\Directory\Models\Auth;

class RefreshToken implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle()
    {
        $auth = Auth::first();

        if (is_null($auth)) {
            return;
        }

        $httpClient = new Client();

        $response = $httpClient->post('https://bitbucket.org/site/oauth2/access_token', [
            'auth' => [config('services.bitbucket.client_id'), config('services.bitbucket.client_secret')],
            'headers' => ['Accept' => 'application/json'],
            'form_params' => [
                'grant_type' => 'refresh_token',
                'refresh_token' => $auth->refreshtoken,
            ],
        ]);

        $body = json_decode($response->getBody(), true);

        $expires = Carbon::now();
        $expires->addSeconds($body['expires_in']);

        $auth->token = $body['access_token'];
        $auth->expiration = $expires;
        $auth->save();

        return $body['access_token'];
    }
}
