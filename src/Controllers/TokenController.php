<?php

namespace Rapture\Directory\Controllers;

use App\Http\Controllers\Controller;
use Rapture\Directory\Jobs\RefreshToken;

class TokenController extends Controller
{
    public function store()
    {
        RefreshToken::dispatchNow();

        return redirect()
            ->route('dashboard.directory.index')
            ->with('status', 'Token successfully reconnected!');
    }
}
