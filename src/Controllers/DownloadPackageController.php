<?php

namespace Rapture\Directory\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rapture\Directory\Models\Package;

class DownloadPackageController extends Controller
{
    public function verify($namespace, $package, $version = 'dev-master')
    {
        $rootPackage = Package::where('name', $namespace . '/' . $package)->first();

        if (is_null($rootPackage)) {
            abort(404);
        }

        $packageVersion = $rootPackage->versions()->where('version', $version)->first();

        if (is_null($packageVersion)) {
            abort(404);
        }

        return response()->download(storage_path($packageVersion->path));
    }
}
