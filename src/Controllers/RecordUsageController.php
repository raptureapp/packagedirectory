<?php

namespace Rapture\Directory\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Rapture\Directory\Models\Package;

class RecordUsageController extends Controller
{
    public function store(Request $request)
    {
        $downloads = collect($request->json())['downloads'];

        foreach ($downloads as $download) {
            $package = Package::where('name', $download['name'])->first();
            $version = $package->versions()->where('version_normalized', $download['version'])->first();
            $version->uses()->create([
                'package_id' => $package->id,
            ]);
        }
    }
}
