<?php

namespace Rapture\Directory\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Rapture\Directory\Exceptions\HookCreationException;
use Rapture\Directory\Exceptions\SyncException;
use Rapture\Directory\Helpers\PackageSetup;
use Rapture\Directory\Models\Auth;

class DirectoryController extends Controller
{
    public function index()
    {
        $auth = Auth::first();

        return view('directory::dashboard.index', compact('auth'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'url' => 'required|url',
        ]);

        $url = collect(explode('/', rtrim(Str::afterLast($request->input('url'), '.org/'), '/')))
            ->take(2)
            ->implode('/');

        $response = null;

        try {
            $response = PackageSetup::sync($url);
        } catch (HookCreationException $e) {
            return redirect()
                ->route('dashboard.directory.index')
                ->with('status', $e->getMessage());
        } catch (SyncException $e) {
            return back()->withErrors($e->getMessage());
        }

        return redirect()
            ->route('dashboard.directory.index')
            ->with('status', 'Package successfully added!');
    }
}
