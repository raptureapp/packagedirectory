<?php

namespace Rapture\Directory\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use GuzzleHttp\Exception\ClientException;
use Laravel\Socialite\Two\InvalidStateException;
use Rapture\Directory\Models\Auth;
use Socialite;

class LoginController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('bitbucket')->redirect();
    }

    /**
     * Obtain the user information from bitbucket.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        try {
            $user = Socialite::driver('bitbucket')->user();
        } catch (InvalidStateException|ClientException $error) {
            return redirect()
                ->route('dashboard.directory.index')
                ->withErrors('You are missing required scopes.');
        }

        $expires = Carbon::now();
        $expires->addSeconds($user->expiresIn);

        $auth = Auth::updateOrCreate([
            'user_id' => auth()->user()->id,
            'provider' => 'bitbucket',
        ], [
            'token' => $user->token,
            'refreshToken' => $user->refreshToken,
            'expiration' => $expires,
            'username' => $user->user['username'],
        ]);

        return redirect()
            ->route('dashboard.directory.index')
            ->with('status', 'Authentication successful!');
    }
}
