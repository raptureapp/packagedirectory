<?php

namespace Rapture\Directory\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Cache;
use Rapture\Directory\Models\Package;
use Rapture\Directory\Models\PackageVersion;

class PackageFeedController extends Controller
{
    public function index()
    {
        $packages = Cache::rememberForever('composer.feed', function () {
            return Package::with('versions')
                ->get()
                ->mapWithKeys(function ($package) {
                    return [
                        $package->name => $package->versions->map(function ($version) use ($package) {
                            $data = [
                                'name' => $package->name,
                                'version' => $version->version,
                                'version_normalized' => $version->version_normalized,
                                'dist' => [
                                    'url' => url('package/' . $package->name . '/' . $version->version),
                                    'type' => 'zip',
                                ],
                            ];

                            if (!is_null($version->composer)) {
                                $data = array_merge((array) json_decode($version->composer), $data);
                            }

                            return $data;
                        }),
                    ];
                });
        });

        return response()->json([
            'notify-batch' => url('usage'),
            'packages' => $packages,
        ]);
    }
    public function show(string $vendor, string $packageName): JsonResponse
    {
        $namespace = $vendor . '/' . $packageName;
        $package = Package::where('name', $namespace)->firstOrFail();

        $versions = $package->versions()
            ->orderByDesc('version_normalized')
            ->get();

        $formattedVersions = $versions->map(function (PackageVersion $version) use ($package) {
            return [
                'name' => $package->name,
                'version' => $version->version,
                'version_normalized' => $version->version_normalized,
                'description' => $package->description,
                'dist' => [
                    'url' => url('package/' . $package->name . '/' . $version->version),
                    'type' => 'zip',
                ],
            ];
        });

        $response = [
            'packages' => [
                $namespace => $formattedVersions,
            ],
        ];

        return response()->json($response);
    }
}
