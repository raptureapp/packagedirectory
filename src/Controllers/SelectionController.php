<?php

namespace Rapture\Directory\Controllers;

use App\Http\Controllers\Controller;

class SelectionController extends Controller
{
    public function index()
    {
        return view('directory::selection.index');
    }
}
