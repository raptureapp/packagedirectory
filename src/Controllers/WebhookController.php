<?php

namespace Rapture\Directory\Controllers;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Composer\Semver\VersionParser;
use Illuminate\Http\Request;
use Rapture\Directory\Jobs\DownloadRelease;
use Rapture\Directory\Jobs\UpdateRelease;
use Rapture\Directory\Models\Package;

class WebhookController extends Controller
{
    public function store(Request $request)
    {
        $push = collect($request->json());
        $package = Package::where('source', $push['repository']['full_name'])->first();
        $devRelease = false;
        $parser = new VersionParser();

        foreach ($push['push']['changes'] as $change) {
            if ($change['new']['type'] === 'tag') {
                $created = Carbon::parse($change['new']['target']['date']);

                DownloadRelease::dispatch($package, $change['new']['target']['hash'], [
                    'version' => $change['new']['name'],
                    'version_normalized' => $parser->normalize($change['new']['name']),
                    'path' => 'https://bitbucket.org/' . $package->source . '/get/' . $change['new']['name'] . '.zip',
                    'created_at' => $created,
                    'updated_at' => $created,
                ]);
            } elseif ($change['new']['type'] === 'branch' && $change['new']['name'] === $package->branch) {
                $devRelease = true;
            }
        }

        if ($devRelease) {
            UpdateRelease::dispatch($package, 'master', [
                'version' => 'dev-master',
                'path' => 'https://bitbucket.org/' . $package->source . '/get/' . $package->branch . '.zip',
            ]);
        }
    }
}
