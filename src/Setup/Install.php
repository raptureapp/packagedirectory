<?php

namespace Rapture\Directory\Setup;

use Rapture\Packages\BaseInstall;

class Install extends BaseInstall
{
    public function handle()
    {
        $this->resourcePermissions('directory');

        $this->addMenu([
            'label' => 'directory::package.plural',
            'route' => 'dashboard.directory.index',
            'icon' => 'folder-tree',
            'permission' => 'directory.index',
            'namespaces' => ['dashboard/directory'],
        ]);
    }
}
