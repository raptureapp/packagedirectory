<?php

namespace Rapture\Directory\Livewire;

use Bitbucket\Client;
use Bitbucket\ResultPager;
use Livewire\Component;
use Rapture\Directory\Exceptions\HookCreationException;
use Rapture\Directory\Exceptions\SyncException;
use Rapture\Directory\Helpers\PackageSetup;
use Rapture\Directory\Models\Auth;
use Rapture\Directory\Models\Package;

class RepoSelector extends Component
{
    public $workspaces = [];
    public $repos = [];
    public $viewing;

    public function syncRepo($url)
    {
        $response = null;

        try {
            $response = PackageSetup::sync($url);
        } catch (HookCreationException $e) {
            session()->flash('status', $e->getMessage());
        } catch (SyncException $e) {
            $this->addError('sync', $e->getMessage());
        }

        if (!is_null($response)) {
            session()->flash('status', 'Package successfully added!');
        }
    }

    public function returnToWorkspaces()
    {
        $this->viewing = null;
    }

    public function selectWorkspace($key)
    {
        $this->viewing = $key;

        $client = $this->prepareSession();

        $paginator = new ResultPager($client);
        $repoFeed = $client->repositories()->workspaces($this->viewing);
        $repos = $paginator->fetchAll($repoFeed, 'list');

        $this->repos = $repos;
    }

    public function mount()
    {
        $client = $this->prepareSession();

        $paginator = new ResultPager($client);
        $workspaceFeed = $client->currentUser();
        $workspaces = $paginator->fetchAll($workspaceFeed, 'listWorkspaces');

        $this->workspaces = $workspaces;
    }

    public function render()
    {
        $existingRepos = Package::pluck('source')->toArray();

        return view('directory::repos.selector', compact('existingRepos'));
    }

    public function prepareSession()
    {
        $auth = Auth::first();

        $client = new Client();
        $client->authenticate(Client::AUTH_OAUTH_TOKEN, $auth->token);

        return $client;
    }
}
