<?php

namespace Rapture\Directory\Livewire;

use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Rapture\Core\Columns\Date;
use Rapture\Core\Columns\ID;
use Rapture\Core\Columns\Relationship;
use Rapture\Core\Columns\Text;
use Rapture\Core\Livewire\DatatableComponent;
use Rapture\Core\Table\Action;
use Rapture\Core\Table\Column;
use Rapture\Directory\Jobs\PackageDelete;
use Rapture\Directory\Models\Package;
use Rapture\Directory\Models\PackageVersion;
use Rapture\Hooks\Facades\Hook;

class PackageTable extends DatatableComponent
{
    public $table = 'dashboard.packages';
    public $searchable = true;

    public function columns()
    {
        return [
            ID::make(),
            Text::make('package', 'Package')
                ->visible()
                ->sortable('name')
                ->render(fn ($package) => '<strong>' . $package->name . '</strong><br>' . $package->description),
            Text::make('name', __('rapture::field.name')),
            Text::make('description', 'Description'),
            Text::make('latest', 'Version')
                ->disableSort()
                ->visible()
                ->query(fn ($query) => $query->withLatestVersion())
                ->render(fn ($package) => $package->latest_version),
            Column::make('uses_count', 'Downloads')
                ->visible()
                ->query(fn ($query) => $query->withCount('uses')),
            Date::make('created_at', __('rapture::field.created')),
            Relationship::make('updated', 'Updated')
                ->visible()
                ->query(function ($query) {
                    $query->addSelect([
                        'updated' => PackageVersion::select('updated_at')
                            ->whereColumn('package_id', 'packages.id')
                            ->where('version', '!=', 'dev-master')
                            ->latest()
                            ->take(1)
                    ]);
                })
                ->render(fn($package) => Carbon::parse($package->updated)->diffForHumans())
                ->defaultSort('desc'),
        ];
    }

    public function actions()
    {
        return [
            Action::make('View Repo')
                ->render(function ($package) {
                    return '<a href="' . $package->repo() . '" target="_blank"><em class="far fa-code-branch"></em></a>';
                }),
            Action::delete('directory'),
        ];
    }

    public function delete($id)
    {
        $package = Package::find($id);
        $package->purge();

        Storage::deleteDirectory('packages/' . $package->name);
        Cache::forget('composer.feed');

        if (is_null($package->hook_id)) {
            return;
        }

        PackageDelete::dispatch($package->source, $package->hook_id);
    }

    public function query()
    {
        return Package::query();
    }
}
