<?php

namespace Rapture\Directory\Livewire;

use Bitbucket\Client;
use Bitbucket\ResultPager;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Livewire\Component;
use Rapture\Directory\Exceptions\HookCreationException;
use Rapture\Directory\Exceptions\SyncException;
use Rapture\Directory\Helpers\PackageSetup;
use Rapture\Directory\Jobs\PackageDelete;
use Rapture\Directory\Models\Auth;
use Rapture\Directory\Models\Package;

class RepoToggle extends Component
{
    public $repo;
    public $exists;

    public function syncRepo()
    {
        if ($this->exists) {
            $this->purgeRepo();
            return;
        }

        $response = null;

        try {
            $response = PackageSetup::sync($this->repo['full_name']);
        } catch (HookCreationException $e) {
            session()->flash('status', $e->getMessage());
            $this->exists = true;
        } catch (SyncException $e) {
            $this->addError($this->repo['full_name'], $e->getMessage());
        }

        if (!is_null($response)) {
            session()->flash('status', 'Package successfully added!');
            $this->exists = true;
        }
    }

    public function purgeRepo()
    {
        $package = Package::where('source', $this->repo['full_name'])->first();
        $package->purge();

        Storage::deleteDirectory('packages/' . $package->name);
        Cache::forget('composer.feed');

        $this->exists = false;

        if (is_null($package->hook_id)) {
            return;
        }

        PackageDelete::dispatch($package->source, $package->hook_id);
    }

    public function mount($repo, $exists)
    {
        $this->repo = $repo;
        $this->exists = $exists;
    }

    public function render()
    {
        return view('directory::repos.toggle');
    }
}
