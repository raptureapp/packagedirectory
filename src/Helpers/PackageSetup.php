<?php

namespace Rapture\Directory\Helpers;

use Carbon\Carbon;
use Composer\Semver\VersionParser;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Rapture\Directory\Exceptions\HookCreationException;
use Rapture\Directory\Exceptions\SyncException;
use Rapture\Directory\Jobs\DownloadRelease;
use Rapture\Directory\Models\Auth;
use Rapture\Directory\Models\Package;

class PackageSetup
{
    public static $url;
    public static $client;
    public static $repo;
    public static $composer;
    public static $versions;
    public static $package;

    public static function sync($url)
    {
        self::$url = $url;

        $auth = Auth::first();
        $exists = Package::where('source', self::$url)->first();

        throw_unless(is_null($exists), SyncException::class, 'Package has already been set up.');

        self::$client = new Client([
            'base_uri' => 'https://api.bitbucket.org/2.0/repositories/',
            'headers' => [
                'Authorization' => 'Bearer ' . $auth->token,
                'Accept' => 'application/json',
            ],
        ]);

        self::fetchRepo();
        self::fetchComposer();
        self::fetchVersions();
        self::createReleases();
        self::attachWebhook();
    }

    public static function makeRequest($url = '', $error = null)
    {
        try {
            $response = self::$client->get(self::$url . $url);
        } catch (ClientException $e) {
            $response = $e->getResponse();
        }

        if (!is_null($error)) {
            throw_unless($response->getStatusCode() === 200, SyncException::class, $error);
        }

        return json_decode($response->getBody()->getContents());
    }

    public static function fetchRepo()
    {
        self::$repo = self::makeRequest('', 'Invalid repository or further permissions required.');
    }

    public static function fetchComposer()
    {
        self::$composer = self::makeRequest('/src/master/composer.json', 'composer.json is inaccessible');
    }

    public static function fetchVersions()
    {
        self::$versions = self::makeRequest('/refs/tags?pagelen=50', 'An error occurred.');
    }

    public static function createReleases()
    {
        $versionNumbers = collect(self::$versions->values);
        $created = Carbon::now();
        $parser = new VersionParser();

        self::$package = Package::create([
            'name' => self::$composer->name,
            'description' => self::$composer->description,
            'source' => self::$url,
            'provider' => 'bitbucket',
            'private' => self::$repo->is_private,
            'branch' => self::$repo->mainbranch->name,
        ]);

        DownloadRelease::dispatch(self::$package, 'master', [
            'version' => 'dev-master',
            'version_normalized' => $parser->normalize('dev-master'),
            'path' => 'https://bitbucket.org/' . self::$url . '/get/' . self::$repo->mainbranch->name . '.zip',
            'created_at' => $created,
            'updated_at' => $created,
        ]);

        foreach ($versionNumbers as $version) {
            $created = Carbon::parse($version->target->date);

            DownloadRelease::dispatch(self::$package, $version->target->hash, [
                'version' => $version->name,
                'version_normalized' => $parser->normalize($version->name),
                'path' => 'https://bitbucket.org/' . self::$url . '/get/' . $version->name . '.zip',
                'created_at' => $created,
                'updated_at' => $created,
            ]);
        }

        Cache::forget('composer.feed');
    }

    public static function attachWebhook()
    {
        try {
            $response = self::$client->post(self::$url . '/hooks', [
                'json' => [
                    'description' => 'Rapture package directory',
                    'url' => url('repo/pushed'),
                    'active' => true,
                    'events' => [
                        'repo:push',
                    ],
                ],
            ]);
        } catch (ClientException $e) {
            $response = $e->getResponse();
        }

        throw_unless($response->getStatusCode() === 200, HookCreationException::class, 'Package was added successfully. You will have to manually add the webhook however.');

        $hook = json_decode($response->getBody()->getContents());

        self::$package->hook_id = $hook->uuid;
        self::$package->save();
    }
}
